import { AfterViewInit, Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { jqxGridComponent } from 'jqwidgets-ng/jqxgrid';

import { SmartTableData } from '../../../@core/interfaces/common/smart-table';
import { TestCoverageData, TestCoverage } from '../../../@core/interfaces/common/testCoverage';
import { map } from 'rxjs/operators'
import { Subject } from 'rxjs/Subject';
import { takeUntil } from 'rxjs/operators';
import { type } from 'os';

@Component({
  selector: 'ngx-test-coverage',
  templateUrl: './test-coverage.component.html',
  styleUrls: ['./test-coverage.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class TestCoverageComponent implements AfterViewInit, OnInit {
  @ViewChild('grid', { static: false }) grid: jqxGridComponent;

  protected readonly unsubscribe$ = new Subject<void>();
  orgCoverage: number = 0;
  columns = [
		{text: 'Id', datafield: 'ApexClassOrTriggerId', width: '25%'},
		{text: 'Name', datafield: 'ApexClassOrTrigger', width: '20%'},
		{text: 'Lines Covered', datafield: 'NumLinesCovered', width: '10%'},
		{text: 'Lines Uncovered', datafield: 'NumLinesUnCovered', width: '10%'},
		{text: 'Percentage', datafield: 'TotalPercentage', width: '10%'},
		{text: 'Last Modified By', datafield: 'Lastmodifiedby'},
  ];
 
  source : any = {
	localdata: [],
	datatype: 'array',
	datafields:
	  [
		{ name: 'ApexClassOrTriggerId', type: 'string' },
		{ name: 'NumLinesCovered', type: 'number' },
		{ name: 'NumLinesUnCovered', type: 'number' },
		{ name: 'ApexClassOrTrigger' , type: 'string', map: 'ApexClassOrTrigger>Name'},
		{ name: 'TotalPercentage' , type: 'number'},
		{ name: 'Lastmodifiedby' , type: 'string', map: 'LastModifiedBy>Name'}
	  ]	  
  }
  dataAdapter: any = new jqx.dataAdapter(this.source);

  constructor(private testCoverageService: TestCoverageData,) { 
	const loadCoverageData = this.testCoverageService.get();
	console.log('get coverage data');
	loadCoverageData
	.pipe(takeUntil(this.unsubscribe$))
	.subscribe((data) => {
		//console.log(data);
		let coverageData: any = [];
		let orgTotalLinesCovered : number = 0;
		let orgTotalLines : number = 0;
		for (let d of Object.values(data)) {
			d.TotalNumberOfLines =  d.NumLinesCovered+d.NumLinesUncovered;
			d.TotalPercentage = d.TotalNumberOfLines > 0 ? (d.NumLinesCovered/d.TotalNumberOfLines)*100 : 0;
			orgTotalLinesCovered = orgTotalLinesCovered+ d.NumLinesCovered;
			orgTotalLines = orgTotalLines+ d.TotalNumberOfLines;
			console.log(d);
			coverageData.push(d);
		}	
		console.log(orgTotalLines);
		this.orgCoverage = (orgTotalLinesCovered/orgTotalLines);
		this.source.localdata = coverageData;
		this.grid.updatebounddata();	
	});
  }

  ngOnInit() {
  } 

  ngAfterViewInit(): void {
	//let value = this.grid.exportdata('xls,grid.xls');
  }

  download(): void {
	let value = this.grid.exportdata('xls','Test Coverage');
  }  

}
