import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrgHealthRoutingModule } from './org-health-routing.module';
import { OrgHealthComponent } from './org-health.component';
import { TestCoverageComponent } from './test-coverage/test-coverage.component';
import { jqxGridModule } from 'jqwidgets-ng/jqxgrid';
import { NbCardModule, NbButtonModule, NbTooltipModule, NbIconModule, NbInputModule, NbTreeGridModule } from '@nebular/theme';
import { ThemeModule } from '../../@theme/theme.module';
import { NgxEchartsModule } from 'ngx-echarts';

@NgModule({
  declarations: [OrgHealthComponent, TestCoverageComponent],
  imports: [
    CommonModule,
    OrgHealthRoutingModule,
    jqxGridModule,
    NbCardModule,
    NbButtonModule,
    NbTreeGridModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    ThemeModule ,
    NgxEchartsModule
  ]
})
export class OrgHealthModule { }
