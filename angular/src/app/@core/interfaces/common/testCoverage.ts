import { Observable } from 'rxjs';
import {NbAuthOAuth2JWTToken} from '@nebular/auth';

export interface TestCoverage {
  ApexClassOrTriggerId: string;
  name: string;
  NumLinesCovered: number;
  NumLinesUncovered: number;
}


export abstract class TestCoverageData {
  abstract get(): Observable<TestCoverage>;
}
