import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { retry, catchError } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User } from '../@core/interfaces/common/users';

import { stringify } from 'querystring';
import { tokenName } from '@angular/compiler';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private userSubject: BehaviorSubject<User>;
    public user: Observable<User>;
    private authentcated : BehaviorSubject<boolean>;

    constructor(
        private router: Router,
        private http: HttpClient
    ) {
        this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
        this.user = this.userSubject.asObservable();
        this.authentcated = new BehaviorSubject<boolean>(false);
    }

    public get userValue(): User {
        return this.userSubject.value;
    }
    

    login(username: string, password: string) {
        return this.http.post<any>(`${environment.apiUrl}/auth/login/prod`, { username, password })
            .pipe(map(user => {
                // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
                user.authdata = window.btoa(username + ':' + password);
                localStorage.setItem('user', JSON.stringify(user));
                this.userSubject.next(user);
                return user;
            }));
    }

    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('user');
        this.authentcated.next(false);
        this.userSubject.next(null);
        this.router.navigate(['/login']);
    } 

    forbiddenAccess() {
        this.router.navigate(['/insufficientAccess']);
    }

    authorize(code : string): Observable<any>{
        //this.loaderService.message.next('Authorizing ....');
        //this.http.get('http://localhost:4300/api/login/oauth2/env/prod').
        return this.http.get<any>(`${environment.apiUrl}/auth/callback?code=`+code).pipe(map(user => {
            // store user details and basic auth credentials in local storage to keep user logged in between page refreshes
            this.userSubject.next(user);
            this.authentcated.next(true); 
            return user;
        }));    
    }


    isAuthenticated(): Observable<boolean> {        
        if(this.userValue){
            this.authentcated.next(true);
        }
        return this.authentcated.asObservable();
    }
    
    authenticate(): void {
        this.authentcated.next(true);
    }

    getToken(): Observable<string> {       
        if(this.userSubject && this.userSubject.getValue()){
            return new BehaviorSubject(this.userSubject.getValue().token);
        }         
    } 
    
    getTokenString(): string {    
        if(this.userSubject && this.userSubject.getValue()){
            return this.userSubject.getValue().token;
        }             
    }     

}