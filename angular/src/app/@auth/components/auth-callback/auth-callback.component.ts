import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute , Router} from '@angular/router';
import { AuthenticationService } from '../../authentication.service';
import { InitUserService } from '../../../@theme/services/init-user.service';

@Component({
  selector: 'ngx-auth-callback',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './auth-callback.component.html',
  styleUrls: ['./auth-callback.component.scss']
})
export class AuthCallbackComponent implements OnInit {
  code : String;
  constructor(private route: ActivatedRoute,
          private router: Router, 
          private authService : AuthenticationService,
          private initUserService: InitUserService) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe( params => {
      this.code = params['code'];
      this.authService.authorize(params['code'])
      .subscribe(user => {    
        this.initUserService.initCurrentUser().subscribe();
        localStorage.setItem('user', JSON.stringify(user));                     
        this.router.navigate(['/']);
        //this.document.location.href = url.redirectURL;
        /*this.authService.authInfo = res;
        this.cookies.set('SENA_auth_accesToken',res.userInfo.accessToken, 1 , "", "", true);
        this.cookies.set('SENA_auth_instanceUrl',res.userInfo.instanceUrl);
        this.router.navigateByUrl('LandingPage');*/
      });      
    });     
  }

}
