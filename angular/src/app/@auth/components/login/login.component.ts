import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class NgxLoginComponent implements OnInit {

  acceptTermCondition: boolean;
  selectedEnvironement: string;

  submitted: boolean = false;
 
  constructor() { }

  ngOnInit(): void {

  }

  login(): void {
    window.location.href = `${environment.apiUrl}/auth/login/${this.selectedEnvironement}`;
  }

  getConfigValue(key: string): any {
    //return getDeepFromObject(this.options, key, null);
  }

  toggleAcceptTermsCondition(checked: boolean) {
    this.acceptTermCondition = checked;
  }  
}
