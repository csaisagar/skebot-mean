export const environment = {
  production: true,
  apiUrl: 'https://infinite-beyond-41339.herokuapp.com/api',
  testUser: {
    token: {},
    email: '',
  },
};
