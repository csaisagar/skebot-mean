var express = require('express');
var router = express.Router();

router.use('/auth', require('../authentication/authRoute.js'));
router.use('/users', require('../users/userRoute'));
router.use('/testCoverage', require('../testCoverage/testCoverageRoute'));
router.use('/metadata', require('../metadata/metadataRoute'));

module.exports = router;