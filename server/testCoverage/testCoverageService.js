const jsforce = require('jsforce');

module.exports = {
    getAll
};

async function getAll(domain, token) {
    var jsforce = require('jsforce');
    var sfConn = new jsforce.Connection({
      serverUrl : `${domain}`,
      sessionId : `${token}`
    });    
    var records = [];
    var error;
    await sfConn.tooling.query("SELECT ApexClassOrTriggerId,ApexClassOrTrigger.Name,LastModifiedBy.name, NumLinesCovered, NumLinesUncovered FROM ApexCodeCoverageAggregate where ApexClassOrTrigger.Name != null and (NumLinesCovered != 0 OR NumLinesUncovered != 0) ORDER BY ApexClassOrTrigger.Name ASC",function(err, results){
        if (err) { error = err.errorCode; return;}        
        records = results.records;		
    });				
    if(error){
        return error;
    } else{
        return records;
    } 
}
