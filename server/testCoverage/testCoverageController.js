var jsforce = require('jsforce');
const testCoverageService = require('./testCoverageService');
const request = require('request');
const jwt = require('jsonwebtoken');

module.exports = {
    getAll    
};


function getAll(req, res, next) {
    testCoverageService.getAll(req.user.domain, req.user.at)
        .then(coverageData => {
             if(coverageData && coverageData.includes('INVALID_SESSION_ID')){
                res.status(401).send('Session Expired');
             }
             res.json(coverageData)
        })
        .catch(err => {if(err) {console.log(err)} next(err)});
}

