var express = require('express');
var router = express.Router();
var authorize = require('../_helpers/authorize');
const testCoverageController = require('./testCoverageController');

// Authorized Routes
//router.get('/current', userController.getCurrent);
router.get('/', authorize(), testCoverageController.getAll);


module.exports = router;