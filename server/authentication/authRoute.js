var express = require('express');
var router = express.Router();
var Role = require('../_helpers/role');
var authorize = require('../_helpers/authorize');
var authController = require('./authController');
//var loginController = require('../controllers/login-Controller');

/* Login to users SFDC. */
//router.get('/env/:envmnt', loginController.Login);

router.get('/login/:envmnt', authController.login);
// TODO Change to POST And test from Angular
router.post('/login/:envmnt',  authController.login);
router.get('/callback',  authController.callback);

// routes
router.post('/authenticate', authController.authenticate);
router.post('/register', authController.register);



module.exports = router;