const jsforce = require('jsforce');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('../_helpers/db');
const Role = require('../_helpers/role');
const User = db.User;

module.exports = {
    authenticate,
    authorizeCode,
    getAll,
    getById,
    getBySFUserId,
    create,
    update,
    delete: _delete
};

async function authenticate({ username, password }) {
    const user = await User.findOne({ username });
    if (user && bcrypt.compareSync(password, user.hash)) {
        const token = jwt.sign({ sub: user.id }, process.env.JWT_SECRET);
        return {
            ...user.toJSON(),
            token
        };
    }
}

async function authorizeCode(code) {    
    const oauth2 = new jsforce.OAuth2({
        clientId : process.env.CLIENT_ID,
        clientSecret : process.env.CLIENT_SECRET,
        redirectUri : process.env.REDIRECT_URI
    });	
    const sfConn = new jsforce.Connection({ oauth2 : oauth2 });
    console.log('**** in Authcode'+code);
    var sfUser;
    try{
        await sfConn.authorize(code ,  async function(err, userInfo) {
            if (err) {  /*throw new Error('Error in authorizing Code'); */ }
            // Now you can get the access token, refresh token, and instance URL information.
            // Save them to establish connection next time.
           /* console.log(sfConn.accessToken);
            console.log(sfConn.refreshToken);
            console.log(sfConn.instanceUrl);
            console.log("User ID: " + userInfo.id);
            console.log("Org ID: " + userInfo.organizationId); */
    /*      sfUser.accessToken = sfConn.accessToken;
            sfUser.Id = userInfo.id;
            sfUser.organizationId = userInfo.organizationId;
            sfUser.instanceUrl = userInfo.instanceUrl;	 */ 
            if(userInfo && sfConn && sfConn.accessToken){
                sfUser = userInfo ;
                sfUser.instanceUrl = sfConn.instanceUrl; 
            }
        });
    } catch(err){
        console.error('error 12121 :'+err); 
        throw new Error(err);
    }
    
    if(sfUser){
        await sfConn.query("select id,firstname,SmallPhotoUrl, lastname, username, email, CompanyName  from user where id = '"+sfUser.id+"'",async function(err, res) {
            if (err) { console.error('error sa:'+err); throw new Error('Error in Getting userInfo');  }
            if(res && res.records){
                sfUser.email = res.records[0].Email;
                sfUser.username = res.records[0].Username;
                sfUser.firstName = res.records[0].FirstName;
                sfUser.lastName = res.records[0].LastName;
                sfUser.companyName = res.records[0].CompanyName;
                sfUser.smallPhotoUrl = res.records[0].SmallPhotoUrl;
            }else{
                return new Error('Error in Getting userInfo');  
            }
        });
        try{
            // Check user in DB with UserId
            const user = await User.findOne({ id: sfUser.id });
            if(user === null){
                console.log('NOT IN DB');
                const user = new User(sfUser);
                await user.save();
            }else{
                // Update last Login 
                Object.assign(user, {...sfUser, lastLoginDate: Date.now() });
                await user.save();
            }
            var role = Role.User;
            if(user.companName == 'Css'){
                role = Role.Admin;
            }
            const token = jwt.sign({ sub: user.id, at: sfConn.accessToken ,domain: sfConn.instanceUrl , role: role}, process.env.JWT_SECRET);
            sfUser.token = token;
        }catch(e){
            throw new Error('Error in creating/Updating user'+ e)
        }

    }

    return sfUser;
}

async function getAll() {
    return await User.find();
}

async function getById(id) {
    return await User.findById(id);
}

async function getBySFUserId(id) {
    return await User.findOne({ id: id });
}

async function create(userParam) {
    // validate
    if (await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // save user
    await user.save();
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);
    await user.save();
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}