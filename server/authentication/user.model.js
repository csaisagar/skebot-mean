const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    username: { type: String, unique: true, required: true },
    email: { type: String, required: true },
    id: { type: String, unique: true, required: true },
    organizationId: { type: String, required: true },
    instanceUrl: { type: String, required: true },
    hash: { type: String },
    firstName: { type: String },
    lastName: { type: String },
    companName: { type: String },
    createdDate: { type: Date, default: Date.now },
    lastLoginDate: { type: Date, default: Date.now },
    smallPhotoUrl: { type: String }
});

schema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform: function (doc, ret) {
        delete ret._id;
        delete ret.hash;
    }
});

module.exports = mongoose.model('User', schema);