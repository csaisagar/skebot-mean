var jsforce = require('jsforce');
const authService = require('./authService');

module.exports = {
    login,
    callback,
    authenticate,
    getAll,
    getCurrent,
    getById,
    register,
    update,
    delete: _delete
};

function login(req,res){
    var env = req.params.envmnt;	
    let oauth2;
	if(env == 'prod'){		
		oauth2 = new jsforce.OAuth2({
			  // you can change loginUrl to connect to sandbox or prerelease env.
			  loginUrl : 'https://login.salesforce.com',	
			  clientId : process.env.CLIENT_ID,
			  clientSecret : process.env.CLIENT_SECRET,
			  redirectUri : process.env.REDIRECT_URI
		});	
	}else if(env == 'sandbox'){		
		oauth2 = new jsforce.OAuth2({
			  // you can change loginUrl to connect to sandbox or prerelease env.
			  loginUrl : 'https://test.salesforce.com',	
			  clientId : process.env.CLIENT_ID,
			  clientSecret : process.env.CLIENT_SECRET,
			  redirectUri : process.env.REDIRECT_URI
        });	        
	}
    //res.status(302).json({authUrl: oauth2.getAuthorizationUrl({ })});
    console.log(oauth2.getAuthorizationUrl({ }));
    res.status(302).redirect(oauth2.getAuthorizationUrl({ }));
}; 

async function callback(req, res, next) {    
    authService.authorizeCode(req.query.code)
        .then(user => user ? res.json(user) : res.status(401).json({ message: 'Failed in Authorization' }))
        .catch(err => next(err));    
}

function authenticate(req, res, next) {
    authService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}

function register(req, res, next) {
    authService.create(req.body)
        .then(() => res.json({success : true}))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    authService.getAll()
        .then(users => { console.log(req.user); res.json(users)})
        .catch(err => next(err));
}

function getCurrent(req, res, next) {
    authService.getById(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function getById(req, res, next) {
    authService.getBySFUserId(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    authService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    authService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}