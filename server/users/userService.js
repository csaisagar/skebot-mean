const jsforce = require('jsforce');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const db = require('../_helpers/db');
const Role = require('../_helpers/role');
const User = db.User;
const https = require('https');


module.exports = {
    getAll,
    getById,
    getBySFUserId,
    getPhotoBySFUserId,
    create,
    update,
    delete: _delete
};

async function getAll() {
    return await User.find();
}

async function getById(id) {
    return await User.findById(id);
}

async function getBySFUserId(id) {
    return await User.findOne({ id: id });
}

async function getPhotoBySFUserId(token) {
    let decoded;
    try{
        decoded = jwt.verify(token, process.env.JWT_SECRET);
        let user = await this.getBySFUserId(decoded.sub);
        console.log(user);
        //let url = `${user.instanceUrl}/secur/frontdoor.jsp?sid=${decoded.at}&retURL=${user.smallPhotoUrl}`;
        let url = `${user.smallPhotoUrl}`;
        console.log(url);
        return await https.get(url);

    var requestSettings = {
        url: 'https://www.google.com/images/srpr/logo11w.png',
        method: 'GET',
        encoding: null
    };

    request(requestSettings, function(error, response, body) {
        res.set('Content-Type', 'image/png');
        res.send(body);
    });         
    }catch(err){
        //Promise.reject(new Error('invalid token'));
        //console.log(err);
    }
}

async function create(userParam) {
    // validate
    if (await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }
    const user = new User(userParam);
    // hash password
    if (userParam.password) {
        user.hash = bcrypt.hashSync(userParam.password, 10);
    }
    // save user
    await user.save();
}

async function update(id, userParam) {
    const user = await User.findById(id);

    // validate
    if (!user) throw 'User not found';
    if (user.username !== userParam.username && await User.findOne({ username: userParam.username })) {
        throw 'Username "' + userParam.username + '" is already taken';
    }

    // hash password if it was entered
    if (userParam.password) {
        userParam.hash = bcrypt.hashSync(userParam.password, 10);
    }

    // copy userParam properties to user
    Object.assign(user, userParam);
    await user.save();
}

async function _delete(id) {
    await User.findByIdAndRemove(id);
}