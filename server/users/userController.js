var jsforce = require('jsforce');
const userService = require('./userService');
const request = require('request');
const jwt = require('jsonwebtoken');

module.exports = {
    getAll,
    getCurrent,
    getById,
    getPhoto,
    register,
    update,
    delete: _delete
};

function register(req, res, next) {
    userService.create(req.body)
        .then(() => res.json({success : true}))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    userService.getAll()
        .then(users => { console.log(req.user); res.json(users)})
        .catch(err => next(err));
}

async function getPhoto(req, res, next){
/*     userService
    .getPhotoBySFUserId(req.query.token)
    .then(res => {
      //console.log(res.Body);
      res.set('Content-Type', 'image/png');
      res.send(res.body);
    }).catch((error) => {
      res.status(400).send({ error: error.message });
    }); */
    decoded = jwt.verify(req.query.token, process.env.JWT_SECRET);
    let user = await userService.getBySFUserId(decoded.sub);    

    var requestSettings = {
        url: `${user.smallPhotoUrl}`,
        method: 'GET',
        encoding: null
    };

    request(requestSettings, function(error, response, body) {
        res.set('Content-Type', 'image/png');
        res.send(body);
    });    
}

function getCurrent(req, res, next) {
    userService.getBySFUserId(req.user.sub)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err)); 
}

function getById(req, res, next) {
    userService.getBySFUserId(req.params.id)
        .then(user => user ? res.json(user) : res.sendStatus(404))
        .catch(err => next(err));
}

function update(req, res, next) {
    userService.update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    userService.delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}