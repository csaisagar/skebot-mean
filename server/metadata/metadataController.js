var jsforce = require('jsforce');
const metadataService = require('./metadataService');
const request = require('request');
const jwt = require('jsonwebtoken');

module.exports = {
    describe  ,
    retrieveMetadata  
};


function describe(req, res, next) {
    metadataService.describe(req.user.domain, req.user.at)
    .then(data => {
            if(data && data.includes('INVALID_SESSION_ID')){
            res.status(401).send('Session Expired');
            }
            res.json(data)
    })
    .catch(err => {if(err) {console.log(err)} next(err)});
}



function retrieveMetadata(req, res, next) {
    metadataService.retrieveMetadata(req.user.domain, req.user.at, req.body)
    .then(data => {
            if(data && data.includes('INVALID_SESSION_ID')){
            res.status(401).send('Session Expired');
            }
            res.json(data)
    })
    .catch(err => {if(err) {console.log(err)} next(err)});
}
