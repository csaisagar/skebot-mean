const jsforce = require('jsforce');

module.exports = {
    describe
};

async function describe(domain, token) {
    var jsforce = require('jsforce');
    var sfConn = new jsforce.Connection({
      serverUrl : `${domain}`,
      sessionId : `${token}`
    });    
    var data = [];
    var error;
    await sfConn.metadata.describe(`${process.env.API_VERSION}`, function(err, results) {	
        if (err) { error = err.errorCode; return;}  
        var arry = getMetadataList(results);
        data = 	results;
    });	    
    if(error){
        return error;
    } else{
        return data;
    } 
}