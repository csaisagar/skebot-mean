var express = require('express');
var router = express.Router();
var authorize = require('../_helpers/authorize');
const metadataController = require('./metadataController');

// Authorized Routes
//router.get('/current', userController.getCurrent);
router.get('/describe', authorize(), metadataController.describe);


module.exports = router;