const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 3001
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
require('dotenv-flow').config({
  default_node_env: 'development'
});
var cors = require('cors'); 
const errorHandler = require('./server/_helpers/error-handler');

const app = express()

app.use(express.static(path.join(__dirname, 'client/dist')))
app.use(cors());
/* app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  next();
}); */
//app.set('views', path.join(__dirname, 'views'))
//app.set('view engine', 'ejs')
//app.get('*', (req, res) => res.render('pages/index'))
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.raw({ 
    type: 'application/octetstream',
    limit: '10mb'
}));

//app.use(cookieParser());
const jwt = require('./server/_helpers/jwt');
// use JWT auth to secure the api
app.use('/api',jwt());
// error handler
app.use(errorHandler);
// API Routes
app.use('/api', require('./server/routes'));

app.get('*', function(req, res) {	
  res.status(200).sendFile(path.join(__dirname + '/client/dist/index.html'));
}); 

// start server
app.listen(PORT, () => console.log(`Listening on ${ PORT } -- ${process.env.MONGODB_URI}`))
  